#include "classifier.h"
#include "esp_log.h"
#include "esp_system.h"

#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/micro/micro_mutable_op_resolver.h"
#include "tensorflow/lite/micro/system_setup.h"
#include "tensorflow/lite/schema/schema_generated.h"

#include "pre_process.h"

static const char *TAG = "classifier";


void* classifier_setup(const unsigned char* model_data)
{
    constexpr int kTensorArenaSize = 300 * 1024;
    EXT_RAM_BSS_ATTR static uint8_t tensor_arena[kTensorArenaSize];

    // inspect model version
    const tflite::Model* model = tflite::GetModel(model_data);
    if (model->version() != TFLITE_SCHEMA_VERSION) {
        ESP_LOGE(TAG, "Model provided is schema version %ld not equal "
                 "to supported version %d.", model->version(), TFLITE_SCHEMA_VERSION);
        return nullptr;
    }

    // define resolver with required operations
    static tflite::MicroMutableOpResolver<8> micro_op_resolver;
    micro_op_resolver.AddAdd();
    micro_op_resolver.AddConv2D();
    micro_op_resolver.AddDepthwiseConv2D();
    micro_op_resolver.AddFullyConnected();
    micro_op_resolver.AddMean();
    micro_op_resolver.AddPad();
    micro_op_resolver.AddReshape();
    micro_op_resolver.AddSoftmax();

    // Build an interpreter to run the model with.
    static tflite::MicroInterpreter interpreter(
        model, micro_op_resolver, tensor_arena, kTensorArenaSize);

    // Allocate memory from the tensor_arena for the model's tensors.
    TfLiteStatus allocate_status = interpreter.AllocateTensors();
    if (allocate_status != kTfLiteOk) {
        ESP_LOGE(TAG, "AllocateTensors() failed");
        return nullptr;
    }

    return &interpreter;
}

void classifier_pre_process(void* interpreter, camera_fb_t *frame)
{
    tflite::MicroInterpreter* _interpreter = reinterpret_cast<tflite::MicroInterpreter*>(interpreter);
    TfLiteTensor* input = _interpreter->input(0);

    const int input_width = input->dims->data[1];
    const int input_height = input->dims->data[2];
    const float q_in_scale = input->params.scale * 255;
    const int32_t q_in_zero = input->params.zero_point;

    // convert rgb565 to rgb888, downsample and apply quantization parameters
    if (pre_process(frame->buf, frame->width, frame->height, input->data.int8, 24, 24, input_width, input_height, q_in_scale, q_in_zero) != 0)
        ESP_LOGE(TAG, "Preprocessing failed");
}

void classifier_infer(void* interpreter)
{
    tflite::MicroInterpreter* _interpreter = reinterpret_cast<tflite::MicroInterpreter*>(interpreter);

    if (kTfLiteOk != _interpreter->Invoke()) {
        ESP_LOGE(TAG, "Invoke failed");
        return;
    }

}

void classifier_post_process(void* interpreter, float* score)
{
    tflite::MicroInterpreter* _interpreter = reinterpret_cast<tflite::MicroInterpreter*>(interpreter);
    TfLiteTensor* output = _interpreter->output(0);

    const float q_out_scale = output->params.scale;
    const int32_t q_out_zero = output->params.zero_point;

    // apply de-quantization parameters
    *score = (output->data.int8[0] - q_out_zero) * q_out_scale;
}
