#ifndef CARS_DETECT_MODEL_DATA_H
#define CARS_DETECT_MODEL_DATA_H

// Model input dimensions
constexpr int kNumCols = 224;
constexpr int kNumRows = 224;
constexpr int kNumChannels = 3;
constexpr int kMaxImageSize = kNumCols * kNumRows * kNumChannels;

extern const unsigned char g_cars_detect_model_data[];
extern const unsigned int g_cars_detect_model_data_len;

#endif /* CARS_DETECT_MODEL_DATA_H */
