/*
 * @Date: 2024-07-02 11:36:22
 * @Author: Zijie Ning zijie.ning@kuleuven.be
 * @LastEditors: Zijie Ning zijie.ning@kuleuven.be
 * @LastEditTime: 2024-07-02 16:08:15
 * @FilePath: /esp/main/main.cpp
 */
#include "esp_log.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_timer.h"

#include "tensorflow/lite/micro/micro_profiler.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/micro/micro_mutable_op_resolver.h"
#include "tensorflow/lite/micro/system_setup.h"
#include "tensorflow/lite/schema/schema_generated.h"

#include "cars_detect_model_data.h"

tflite::MicroInterpreter *g_interpreter;

bool setup()
{
    constexpr int kTensorArenaSize = 7000 * 1024;
    EXT_RAM_BSS_ATTR static uint8_t tensor_arena[kTensorArenaSize];

    // inspect model version
    const tflite::Model* model = tflite::GetModel(g_cars_detect_model_data);
    if (model->version() != TFLITE_SCHEMA_VERSION) {
        printf("Model provided is schema version %ld not equal "
                 "to supported version %d.", model->version(), TFLITE_SCHEMA_VERSION);
        return false;
    }

    // define resolver with required operations
    static tflite::MicroMutableOpResolver<10> micro_op_resolver;
    micro_op_resolver.AddAveragePool2D();
    micro_op_resolver.AddAdd();
    micro_op_resolver.AddConv2D();
    micro_op_resolver.AddDepthwiseConv2D();
    micro_op_resolver.AddFullyConnected();
    micro_op_resolver.AddMean();
    micro_op_resolver.AddPad();
    micro_op_resolver.AddReshape();
    micro_op_resolver.AddSoftmax();
    micro_op_resolver.AddLogistic();

    // Build an interpreter to run the model with.
    static tflite::MicroInterpreter interpreter(
        model, micro_op_resolver, tensor_arena, kTensorArenaSize);

    // Allocate memory from the tensor_arena for the model's tensors.
    TfLiteStatus allocate_status = interpreter.AllocateTensors();
    if (allocate_status != kTfLiteOk) {
        printf("AllocateTensors() failed");
        return false;
    }

    g_interpreter = &interpreter;

    return true;
}

void main_task(void)
{
    TfLiteTensor* input = g_interpreter->input(0);
    TfLiteTensor* output = g_interpreter->output(0);

    vTaskDelay(10000 / portTICK_PERIOD_MS);

    for (int i=0; i<1000; i++) {
        const int64_t start_time = esp_timer_get_time();
        // preprocess
        for(int i=0; i<kMaxImageSize; i++) {
            input->data.int8[i] = 0;
            //input->data.f[i] = 0;
        }

        // run inference
        if (kTfLiteOk != g_interpreter->Invoke()) {
            printf("Invoke failed");
            return;
        }
        const int64_t stop_time = esp_timer_get_time();
        printf("Inference time = %d us\r\n", (int)(stop_time - start_time));
        vTaskDelay(1);
    }

    while(true) {
         vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

// main entry point of esp ecosystem
extern "C" void app_main()
{
    if (!setup()) {
        printf("Setup failed\n");
        while(1) {
            vTaskDelay(1);
        }
    }

    xTaskCreate((TaskFunction_t)&main_task, "main", 32 * 1024, NULL, 8, NULL);
    vTaskDelete(NULL);
}
