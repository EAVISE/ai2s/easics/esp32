# Deployment of MobileNetV2 on XIAO-ESP32-S3

This project deploys a MobileNetV2 network on XIAO-ESP32-S3.

Model is obtained from keras:

```python
model = tf.keras.applications.mobilenet_v2.MobileNetV2(
    include_top=True,
    weights='imagenet',
    input_tensor=None,
    input_shape=None,
    pooling=None,
    classes=1000
)
```

Workflow tool chain is available using VSCode plugin "ESP-IDF".

Main code will inference the model for 1000 times.
