'''
Date: 2024-06-25 16:02:18
Author: Zijie Ning zijie.ning@kuleuven.be
LastEditors: Zijie Ning zijie.ning@kuleuven.be
LastEditTime: 2024-07-02 15:46:12
FilePath: /pycoral/tflite_quant.py
'''
# https://www.tensorflow.org/lite/performance/post_training_integer_quant?hl=zh-cn
import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.keras.optimizers import Adam

import numpy as np
import pathlib

# Construct a tf.data.Dataset
train_images = tfds.load('imagenette', split='train[:100]', shuffle_files=True)
train_images = train_images.shuffle(1000).batch(1).prefetch(100)

# Instantiate the model
model = tf.keras.applications.mobilenet_v2.MobileNetV2(
    include_top=True,
    weights='imagenet',
    input_tensor=None,
    input_shape=None,
    pooling=None,
    classes=1000
)

# Compile the model
model.compile(optimizer=Adam(),
              loss='categorical_crossentropy',
              metrics=['accuracy'])

# saved_model_dir = "mobilenet_v2"
MODEL_INPUT_HEIGHT = 224
MODEL_INPUT_WIDTH = 224

def representative_data_gen():
  # for input_value in tf.data.Dataset.from_tensor_slices(train_images).batch(1).take(100):
  for input_value in train_images.take(100):
    input_value = input_value['image']
    input_value = tf.image.resize(input_value, [MODEL_INPUT_HEIGHT, MODEL_INPUT_WIDTH])
    preprocess_input = tf.keras.applications.mobilenet_v2.preprocess_input
    input_value = preprocess_input(input_value)
    input_value = tf.cast(input_value, tf.float32)
    yield [input_value]

# Convert the model
converter = tf.lite.TFLiteConverter.from_keras_model(model) # path to the SavedModel directory
model.summary()
converter.optimizations = [tf.lite.Optimize.DEFAULT]
converter.representative_dataset = representative_data_gen

# Ensure that if any ops can't be quantized, the converter throws an error
converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
# Set the input and output tensors to uint8 (APIs added in r2.3)
converter.inference_input_type = tf.int8
converter.inference_output_type = tf.int8

tflite_model_quant = converter.convert()

interpreter = tf.lite.Interpreter(model_content=tflite_model_quant)
input_type = interpreter.get_input_details()[0]['dtype']
print('input: ', input_type)
output_type = interpreter.get_output_details()[0]['dtype']
print('output: ', output_type)

# Save the model.
tflite_models_dir = pathlib.Path("output/")
tflite_models_dir.mkdir(exist_ok=True, parents=True)
tflite_model_quant_file = tflite_models_dir/"mobilenet_v2_quant.tflite"
tflite_model_quant_file.write_bytes(tflite_model_quant)

# xxd -i mobilenet_v2_quant.tflite > "mobilenet_v2_quant.cpp"